// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:

console.log("code #1 output")
let grade = (a) => {
    if (a <= 74) {
        console.log('Sorry! Your quarterly average is ' + a + '. You have received a "Failed" mark.');
    } else if (a <= 80) {
        console.log('Congratulations! Your quarterly average is ' + a + '. You have received a "Beginner" mark.')
    } else if (a <= 85) {
        console.log('Congratulations! Your quarterly average is ' + a + '. You have received a "Developing" mark.')
    } else if (a <= 90) {
        console.log('Congratulations! Your quarterly average is ' + a + '. You have received a "Above Average" mark.')
    } else if (a <= 100) {
        console.log('Congratulations! Your quarterly average is ' + a + '. You have received a "Advanced" mark.')
    } else {
        console.log('Not a valid input');
    }
}

grade(74)
grade(80)
grade(85)
grade(90)
grade(100)
grade(110)


/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:

console.log(" ")
console.log("code #2 output")
for (let count = 0; count <= 300; count++) {
    count % 2 == 0 ? console.log(`${count} - even`) : console.log(`${count} - odd`);;
}

/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

let hero = `{
	"heroName": "Aldous",
	"origin": "Minoan Empire",
	"description": "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.",
	"skills": {
		      "skill1": "Soul Steal",
		      "Skill2": "Explosion",
		      "Skill3": "Chase Fate"
		  }
}`

console.log(hero)
console.log(JSON.parse(hero))